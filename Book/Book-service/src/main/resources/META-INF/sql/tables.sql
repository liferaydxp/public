create table cignext_Book (
	uuid_ VARCHAR(75) null,
	bookId LONG not null primary key,
	bookName VARCHAR(75) null,
	description VARCHAR(75) null,
	authorName VARCHAR(75) null,
	isbn INTEGER,
	price INTEGER
);
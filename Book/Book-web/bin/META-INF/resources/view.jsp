<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="com.book.model.Book"%>
<%@page import="java.util.List"%>
<%@page import="com.book.service.BookLocalServiceUtil"%>
<%@page import="javax.portlet.WindowState"%>

<%@ include file="/init.jsp" %>


<portlet:renderURL var="viewaddPageURL">
	<portlet:param name="jspPage" value="/add_book.jsp"/>
</portlet:renderURL>

<a href="<%=viewaddPageURL.toString()%>">Add Book</a>

<liferay-ui:success key="added-book" message="Book Added Successfully"/>
<liferay-ui:success key="deleted-book" message="Book Deleted Successfully"/>
<liferay-ui:success key="updated-book" message="Book Updated Successfully"/>
 
 <% 
 //PortletURL iteratorURL = renderResponse.createRenderURL();
	 List<Book> bookList = BookLocalServiceUtil.getBooks(-1,-1);
 %>
 
<p>
     <b><liferay-ui:message key="My Book List"/></b>
 </p>
 
<liferay-ui:search-container delta="2" emptyResultsMessage="No Books Found!!">
    <liferay-ui:search-container-results  results="<%= ListUtil.subList(bookList, -1, -1) %>" />
    <liferay-ui:search-container-row className="com.book.model.Book" keyProperty="bookId" modelVar="book">
        <liferay-ui:search-container-column-text name="Title" value="${book.bookName}" />
        <liferay-ui:search-container-column-text name="Author" value="${book.authorName}" />
        <liferay-ui:search-container-column-text name="ISBN" value="${book.isbn}" />
        <liferay-ui:search-container-column-text name="Price" value="${book.price}" />
        <liferay-ui:search-container-column-jsp path="/action.jsp" align="right" /> 
    </liferay-ui:search-container-row>
    <liferay-ui:search-iterator />
</liferay-ui:search-container>
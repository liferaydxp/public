package com.example.portlet;

import com.book.model.Book;
import com.book.service.BookLocalServiceUtil;
import com.example.util.ActionUtil;
import com.example.util.WebKeys;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletSession;
import javax.portlet.ProcessAction;

import org.osgi.service.component.annotations.Component;

@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=Book-web Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class BookWebmvcportletPortlet extends MVCPortlet {
	@ProcessAction(name = "addBook")
    public void addBook(ActionRequest actionRequest,ActionResponse actionResponse) throws SystemException {
        Book book = ActionUtil.bookFromRequest(actionRequest);
 
        //Calling service method to persist book.
        BookLocalServiceUtil.addBook(book);
        SessionMessages.add(actionRequest, "added-book");
    }
 
    @ProcessAction(name = "deleteBook")
    public void deleteBook(ActionRequest actionRequest,ActionResponse actionResponse) throws SystemException, PortalException {
        long bookId = ParamUtil.getLong(actionRequest, "bookId");
        BookLocalServiceUtil.deleteBook(bookId);
        SessionMessages.add(actionRequest, "deleted-book");
    }
 
    @ProcessAction(name = "viewBook")
    public void viewBook(ActionRequest actionRequest,ActionResponse actionResponse) throws SystemException, PortalException {
        long bookId = ParamUtil.getLong(actionRequest, "bookId");
        Book book = BookLocalServiceUtil.getBook(bookId);
        actionRequest.setAttribute(WebKeys.BOOK_ENTRY, book);
        //Show view_book.jsp
        actionResponse.setRenderParameter("jspPage", "/view_book.jsp");
    }
 
    @ProcessAction(name = "updateBook")
    public void updateBook(ActionRequest actionRequest,ActionResponse actionResponse) throws SystemException, PortalException {
        long bookId = (Long) actionRequest.getPortletSession().getAttribute(WebKeys.BOOK_ID,PortletSession.PORTLET_SCOPE);
        Book book = ActionUtil.bookFromRequest(actionRequest);
        book.setBookId(bookId);
        /**
         * Calling service method to update book.
         * While calling update method we must have primary key in the passing object.
         */
        BookLocalServiceUtil.updateBook(book);
        SessionMessages.add(actionRequest, "updated-book");
 
    }
 
    @ProcessAction(name = "viewEdit")
    public void viewEdit(ActionRequest actionRequest,ActionResponse actionResponse) throws SystemException, PortalException {
        long bookId = ParamUtil.getLong(actionRequest, "bookId",0);
        Book book = BookLocalServiceUtil.getBook(bookId);
        actionRequest.setAttribute(WebKeys.BOOK, book);
        //Show edit_book.jsp
        actionResponse.setRenderParameter("jspPage", "/edit_book.jsp");
        actionRequest.getPortletSession().setAttribute(WebKeys.BOOK_ID, book.getBookId(),PortletSession.PORTLET_SCOPE);
 
    }
    private Log _log = LogFactoryUtil.getLog(BookWebmvcportletPortlet.class.getName());
}
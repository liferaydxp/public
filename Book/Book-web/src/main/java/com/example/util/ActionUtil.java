package com.example.util;

import com.book.model.Book;
import com.book.model.impl.BookImpl;
import com.liferay.portal.kernel.util.ParamUtil;
import javax.portlet.ActionRequest;
public class ActionUtil {

	  public static Book bookFromRequest(ActionRequest request) {
		  Book book = new BookImpl();
		  book.setAuthorName(ParamUtil.getString(request, "authorName",""));
		  book.setBookName(ParamUtil.getString(request, "bookName",""));
		  book.setIsbn(ParamUtil.getInteger(request, "isbn",0));
		  book.setPrice(ParamUtil.getInteger(request, "price",0));
		  book.setDescription(ParamUtil.getString(request, "description",""));
		
		  return book;
	  }
}
